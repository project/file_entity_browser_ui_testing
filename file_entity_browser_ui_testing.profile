<?php
/**
 * @file
 * Enables modules and site configuration for a file_entity_browser_ui_testing
 * site installation.
 */

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function file_entity_browser_ui_testing_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
//  $form['#submit'][] = 'file_entity_browser_ui_testing_form_install_configure_submit';

  $form['site_information']['site_name']['#default_value'] = t('File Entity Browser UI Testing');
  $form['site_information']['site_mail']['#default_value'] = t('test@localhost.com');

  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = t('test@localhost.com');

  $form['update_notifications']['update_status_module']['#default_value'] = array();
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function file_entity_browser_ui_testing_form_install_configure_submit($form, FormStateInterface $form_state) {
//  $site_mail = $form_state->getValue('site_mail');
//  ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}
